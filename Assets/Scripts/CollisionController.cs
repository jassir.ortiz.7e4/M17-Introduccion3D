using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CollisionController : MonoBehaviour
{
    [SerializeField] ScreenManager screen;
    Vector3 _initialPosPlayer;
    
    void Start()
    {
        _initialPosPlayer = transform.position;
    }

    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Lava")) {
            transform.position = _initialPosPlayer;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            screen.score++;
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("FinalCoin"))
        {
            screen.score = 0;
            transform.position = _initialPosPlayer;
        }
    }
}
